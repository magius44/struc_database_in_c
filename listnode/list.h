#include <stdint.h>

typedef struct elem_t
{

    float value;
    struct elem_t *next;
} elem_t;

typedef struct list_t
{
    int size;
    elem_t *head;
} list_t;

list_t new_list();
void push_elem(list_t *list, float value);
void print_list(list_t *list);
void remove_elem(list_t *list, int idx);
void delete_list(list_t *list);
int find(list_t *list, float element);
void print_array(list_t *list, int idx);
float get_elem(list_t *list, int idx);
list_t *split(list_t *list, int n);