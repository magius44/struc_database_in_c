#include <stdlib.h>
#include <stdio.h>
#include "list.h"

list_t new_list()
{
    list_t l = {
        .size = 0,
        .head = NULL};

    return l;
}

void push_elem(list_t *list, float value)
{
    elem_t *elm = malloc(sizeof(elem_t)); // alloue une case memoire de 12 octet
    elm->value = value;
    elm->next = NULL;

    if (list->head == NULL)
    {
        list->head = elm;
        list->size++;
        return;
    }

    elem_t *prev_elemnt = list->head;
    while (prev_elemnt->next)
    {
        prev_elemnt = prev_elemnt->next;
    }
    prev_elemnt->next = elm;
    list->size++;
}

void print_list(list_t *list)
{

    elem_t *e = list->head;

    while (e != NULL)
    {
        printf("%f -> ", e->value);
        e = e->next;
    }
    printf("NULL\n");
}

void remove_elem(list_t *list, int idx)
{
    if (list->head != NULL) //verifie qu'il ya un elemnt dans la list
    {
        int i = 0;
        elem_t *e = list->head;
        elem_t *tmp;
        if (idx == 0)
        {

            elem_t *elmAsupp = list->head;
            list->head = list->head->next;
            free(elmAsupp);
        }

        if (idx == list->size)
        {
            elem_t *prev_elemnt = list->head;
            while (prev_elemnt->next)
            {
                tmp = prev_elemnt;
                prev_elemnt = prev_elemnt->next;
            }
            tmp->next = NULL;
            free(prev_elemnt);
            list->size--;
        }

        if (idx < list->size)
        {
            elem_t *prev_elemnt = NULL;
            tmp = list->head;
            while (i != idx)
            {
                prev_elemnt = tmp;
                tmp = tmp->next;
                i++;
            }
            prev_elemnt->next = tmp->next;
            free(tmp);
            list->size--;
        }
    }
}

void delete_list(list_t *list)
{
    elem_t *tmp = list->head;

    while (tmp != NULL)
    {
        elem_t *next = tmp->next;
        free(tmp);
    }
    list->head = NULL;
}

int find(list_t *list, float element)
{
    if (list->head != NULL)
    {
        int idx = 0;
        elem_t *current = list->head;
        while (current->value != element)
        {
            current = current->next;
            idx++;
        }
        return idx;
    }
}

float get_elem(list_t *list, int idx)
{

    if (list->head != NULL) //verifie qu'il ya un elemnt dans la list
    {
        int i = 0;
        elem_t *elmArecup = list->head;

        while (i != idx)
        {
            elmArecup = elmArecup->next;
            i++;
        }

        return elmArecup->value;
    }
    return 0;
}
void print_array(list_t *list, int idx)
{

    for (int i = 0; i < idx; i++)
    {
        print_list(&list[i]);
    }
}
list_t *split(list_t *list, int n)
{
    if (list->size % n == 0)
    {
        int deb = 0;
        int fin = list->size / n;
        list_t *mes_listes = malloc(sizeof(list_t) * n);
        for (int i = 0; i < n; i++)
        {
            //boucle qui   va de la 1 liste a la n-ieme liste qu'on doit crée

            list_t tmp = new_list();

            for (int i = deb; i < fin; i++)
            {
                push_elem(&tmp, get_elem(list, i));
            }
            deb = fin;
            fin += fin;
            mes_listes[i] = tmp;
        }
        return mes_listes;
    }
};
