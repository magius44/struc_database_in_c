#include <stdlib.h>
#include <stdio.h>
#include "list.h"

int main()
{
    list_t l = new_list();
    printf("Liste vide \n");
    print_list(&l);
    printf("---------------\n");

    printf("ajout d'un element  dans la Liste\n");
    print_list(&l);
    push_elem(&l, 1);
    print_list(&l);
    printf("---------------\n");

    printf("ajout d'autre  element \n");
    print_list(&l);
    push_elem(&l, 2);
    push_elem(&l, 3);
    push_elem(&l, 4);
    push_elem(&l, 5);
    print_list(&l);
    printf("---------------\n");

    printf("suppression d'un element au debut\n");
    print_list(&l);
    remove_elem(&l, 1);
    print_list(&l);
    printf("---------------\n");

    printf("suppression du dernier element ajouté au vector\n");
    print_list(&l);
    remove_elem(&l, 1);
    print_list(&l);
    printf("---------------\n");

    printf("suppression du  vecteur\n");
    print_list(&l);
    print_list(&l);
    printf("---------------\n");

    printf("Chercher un element qui a une valuer de 4.0\n");
    print_list(&l);
    printf("il se trouve a a la positon %i \n", find(&l, 4.0));
    printf("---------------\n");

    printf("Split les list en petites listes\n");
    push_elem(&l, 6);
    print_list(&l);
    print_array(split(&l, 2), 2);
}