#include <stdint.h>

typedef struct dl_node_t
{
    int size;
    int capacity;
    float *value;

} vector;

vector new_vector();
void vec_push(vector *vec, float value);
void vec_print(vector *vec);
void vec_remove_elem(vector *vec, int index);
void vec_delete(vector *vec);
int vec_size(vector *vec);
