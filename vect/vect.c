#include <stdlib.h>
#include <stdio.h>
#include "vect.h"

vector new_vector()
{
    vector vec = {
        .size = 0,
        .capacity = 0,
        .value = NULL};

    return vec;
}

void vec_push(vector *vec, float value)
{
    if (vec->size == 0) //cas si le vecteur est vide
    {
        vec->capacity = 3;
        vec->value = malloc(sizeof(float) * vec->capacity);
        vec->value[0] = value;
    }
    else
    {

        if (vec->size == vec->capacity)
        {
            vec->capacity += 3; //augment la capacité de 3 case memoire
            vec->value = realloc(vec->value, sizeof(float) * vec->capacity);
        }

        vec->value[vec->size] = value;
    }
    vec->size++;
}
void vec_print(vector *vec)
{
    printf("[ ");
    for (int i = 0; i < vec->capacity; i++)
    {

        if (i != vec->capacity - 1)
        {
            printf("%f, ", vec->value[i]);
        }
        else
        {
            printf("%f", vec->value[i]);
        }
    }
    printf("]\n");
};

void vec_remove_elem(vector *vec, int index)
{
    if (index < vec->size)
    {
        vec->value[index] = 0;
        // redecale les  valeur suivante du vecteur
        for (int i = index; i < vec->size; i++)
        {
            vec->value[i] = vec->value[i + 1];
        }

        //retreci le vecteur
        vec->capacity = vec->capacity - 1;
        if (vec->capacity % vec->size == 0)
        {
            vec->capacity -= vec->capacity / vec->size;
            vec->value = realloc(vec->value, sizeof(float) * vec->capacity);
        }

        vec->size--;
    }
};
void vec_delete(vector *vec)
{
    free(vec->value);
    vec->value = NULL;
    vec->size = 0;
    vec->capacity = 0;
};

int vec_size(vector *vec)
{
    return vec->size;
}