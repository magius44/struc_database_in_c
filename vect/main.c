#include <stdlib.h>
#include <stdio.h>
#include "vect.h"

int main()
{

    vector vec = new_vector();
    printf("Vecteur vide \n");
    vec_print(&vec);
    printf("---------------\n");

    printf("ajout d'un element \n");
    vec_push(&vec, 1);
    vec_print(&vec);
    printf("---------------\n");

    printf("ajout d'autre  element \n");
    vec_push(&vec, 2);
    vec_push(&vec, 3);
    vec_push(&vec, 4);
    vec_push(&vec, 5);
    vec_print(&vec);
    printf("---------------\n");

    printf("suppression d'un element au debut\n");
    vec_print(&vec);
    vec_remove_elem(&vec, 0);
    vec_print(&vec);
    printf("---------------\n");

    printf("suppression du dernier element ajouté au vector\n");
    vec_print(&vec);
    vec_remove_elem(&vec, vec_size(&vec) - 1);
    vec_print(&vec);
    printf("---------------\n");

    printf("suppression du  vecteur\n");
    vec_print(&vec);
    vec_delete(&vec);
    vec_print(&vec);
}