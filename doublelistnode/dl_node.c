#include <stdlib.h>
#include <stdio.h>
#include "dl_node.h"

dl_list_t new_list()
{
    dl_list_t l = {
        .size = 0,
        .head = NULL,
        .last = NULL};

    return l;
};

void dl_push(dl_list_t *list, float value)
{
    dl_node_t *elm = malloc(sizeof(dl_node_t));
    elm->value = value;
    elm->next = NULL;
    elm->previous = list->last;

    if (list->last)
    {
        list->last->next = elm;
    }
    else
    {
        list->head = elm;
    }
    list->last = elm;

    /* Pour je nesais quelle raison cette version 
    ne marche pas mais l'inverse oui
     if (list->head)
    {
        list->head = elm;
    }
    else
    {
        list->last->next = elm;
    }
    list->last = elm; */
    list->size++;
};
void dl_print(dl_list_t *list)
{
    dl_node_t *e = list->head;

    while (e != NULL)
    {
        printf("%f -> ", e->value);
        e = e->next;
    }
    printf("NULL\n");
};
void dl_remove_elem(dl_list_t *list, int index)
{
    if (index > (list->size - 1))
    {
        return;
    }

    int curr_index = 0;
    dl_node_t *elem_to_remove = list->head;
    if (index == 0)
    {

        list->head = elem_to_remove->next; //place le pointeur head sur l'element qui suis celui que l'on veux supprimer
        list->head->previous = NULL;       //place le pointeur du previous du nouveau head sur NULL
        free(elem_to_remove);
        list->size--;
        return;
    }

    while (curr_index < index)
    {

        elem_to_remove = elem_to_remove->next;
        curr_index++;
    }

    list->size--;
    dl_node_t *previous = elem_to_remove->previous;
    previous->next = elem_to_remove->next;
    free(elem_to_remove);
};
void dl_delete(dl_list_t *list)
{
    dl_node_t *tmp;
    dl_node_t *pelem = list->head;
    while (pelem)
    {
        tmp = pelem;
        pelem = pelem->next;
        free(tmp);
    }
    list->head = NULL;
    list->last = NULL;
};

int dl_length(dl_list_t *list)
{
    return list->size;
}