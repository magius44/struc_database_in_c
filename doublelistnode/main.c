#include <stdlib.h>
#include <stdio.h>
#include "dl_node.h"

int main()
{
    dl_list_t l = new_list();
    printf("double Liste vide \n");
    dl_print(&l);
    printf("`---------------\n");
    dl_push(&l, 1);
    dl_push(&l, 2);
    dl_push(&l, 3);
    dl_push(&l, 4);
    dl_push(&l, 5);
    printf("double Liste avec 5 element \n");
    dl_print(&l);
    printf("`---------------\n");

    printf("effacer le dernier element de la double liste \n");
    dl_print(&l);
    dl_remove_elem(&l, dl_length(&l) - 1);
    dl_print(&l);
    printf("`---------------\n");

    printf("effacer le premier element de la double liste \n");
    dl_print(&l);
    dl_remove_elem(&l, 0);
    dl_print(&l);
    printf("`---------------\n");

    printf("effacer un element du milieu de la liste \n");
    dl_print(&l);
    dl_remove_elem(&l, 1);
    dl_print(&l);
    printf("`---------------\n");

    printf("effacer la liste\n");
    dl_print(&l);
    dl_delete(&l);
    dl_print(&l);
}