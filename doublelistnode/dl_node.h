#include <stdint.h>

typedef struct dl_node_t
{
    float value;
    struct dl_node_t *previous;
    struct dl_node_t *next;

} dl_node_t;

typedef struct
{
    int size;
    dl_node_t *head;
    dl_node_t *last;
} dl_list_t;

dl_list_t new_list();
void dl_push(dl_list_t *list, float value);
void dl_print(dl_list_t *list);
void dl_remove_elem(dl_list_t *list, int index);
int dl_length(dl_list_t *list);
void dl_delete(dl_list_t *list);
